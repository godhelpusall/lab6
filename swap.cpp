#include <iostream>
using namespace std;

void print_array(int arg[], int length);
void swap(int ar[],int pos1, int pos2 );

int main(){
  int number[] = {1,2,3};
  print_array(number,3);
 
 
  
  return 0;
}

void print_array(int arg[],int length){
  
  cout<<"Before swapping: ";
  cout<<"{ ";
  for (int i=0;i<length;i++){
    cout<<arg[i]<<" ";
  }
  cout<<"}"<<"\n";
  cout<<"After swapping: ";
  swap(arg,0,1);
  cout<<"{ ";
  for (int i=0;i<length;i++){
    cout<<arg[i]<<" ";
  }
  cout<<"}";
  
  
}

void swap(int ar[],int pos1, int pos2 ){
  int temp;

    temp = ar[0];
    ar[0] = ar[1];
    ar[1]=temp;
    
}
