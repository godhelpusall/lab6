#include <iostream>
using namespace std;

int main(int args, char *argv[]){

  cout << "Program name: " << argv[0] << endl;
  cout << "Called with " << (args -1) << " arguments: ";

  for (int i=1; i < args; i++){
    cout << "'" << argv[i] << "' ";
  }

  return 0;
}
