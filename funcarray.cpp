#include <iostream>
using namespace std;

void ToPrint (int numbers [], int size)
{
  cout << "[ ";
  for (int i =0; i < size; i++)
  {
    cout << numbers [i]<< " ";
  }
  cout << "]\n";
}

int main ()
{

  int size;
  int count = 0;

  cout << "Please enter the size of the array: ";
  cin >> size;

  int numbers [size];
  while (count < size)
    {
      cout << "Enter the number for postion " << count << " ";
      cin >> numbers [count];
      count++;
      
    }
  ToPrint (numbers, size);
  
}
